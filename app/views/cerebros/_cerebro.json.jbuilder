json.extract! cerebro, :id, :flavor, :created_at, :updated_at
json.url cerebro_url(cerebro, format: :json)
