class CerebrosController < ApplicationController
  before_action :set_cerebro, only: [:show, :edit, :update, :destroy]

  # GET /cerebros
  # GET /cerebros.json
  def index
    @cerebros = Cerebro.all
  end

  # GET /cerebros/1
  # GET /cerebros/1.json
  def show
  end

  # GET /cerebros/new
  def new
    @cerebro = Cerebro.new
  end

  # GET /cerebros/1/edit
  def edit
  end

  # POST /cerebros
  # POST /cerebros.json
  def create
    @cerebro = Cerebro.new(cerebro_params)

    respond_to do |format|
      if @cerebro.save
        format.html { redirect_to @cerebro, notice: 'Cerebro was successfully created.' }
        format.json { render :show, status: :created, location: @cerebro }
      else
        format.html { render :new }
        format.json { render json: @cerebro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cerebros/1
  # PATCH/PUT /cerebros/1.json
  def update
    respond_to do |format|
      if @cerebro.update(cerebro_params)
        format.html { redirect_to @cerebro, notice: 'Cerebro was successfully updated.' }
        format.json { render :show, status: :ok, location: @cerebro }
      else
        format.html { render :edit }
        format.json { render json: @cerebro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cerebros/1
  # DELETE /cerebros/1.json
  def destroy
    @cerebro.destroy
    respond_to do |format|
      format.html { redirect_to cerebros_url, notice: 'Cerebro was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cerebro
      @cerebro = Cerebro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cerebro_params
        params.require(:cerebro).permit(:flavor,:iq,:fresh,:zombie_id)
    end
end
