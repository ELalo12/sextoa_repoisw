class AddIqToCerebro < ActiveRecord::Migration[5.0]
  def change
    add_column :cerebros, :iq, :integer
  end
end
