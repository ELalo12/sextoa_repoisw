class CreateCerebros < ActiveRecord::Migration[5.0]
  def change
    create_table :cerebros do |t|
      t.string :flavor

      t.timestamps
    end
  end
end
