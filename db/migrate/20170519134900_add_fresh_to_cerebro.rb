class AddFreshToCerebro < ActiveRecord::Migration[5.0]
  def change
    add_column :cerebros, :fresh, :boolean
  end
end
