class AddZombieToCerebro < ActiveRecord::Migration[5.0]
  def change
    add_reference :cerebros, :zombie, foreign_key: true
  end
end
