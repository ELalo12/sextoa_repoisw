Rails.application.routes.draw do
  resources :cerebros
  resources :brains
  resources :zombies
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
