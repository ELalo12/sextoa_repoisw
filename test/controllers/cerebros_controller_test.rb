require 'test_helper'

class CerebrosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cerebro = cerebros(:one)
  end

  test "should get index" do
    get cerebros_url
    assert_response :success
  end

  test "should get new" do
    get new_cerebro_url
    assert_response :success
  end

  test "should create cerebro" do
    assert_difference('Cerebro.count') do
      post cerebros_url, params: { cerebro: { flavor: @cerebro.flavor } }
    end

    assert_redirected_to cerebro_url(Cerebro.last)
  end

  test "should show cerebro" do
    get cerebro_url(@cerebro)
    assert_response :success
  end

  test "should get edit" do
    get edit_cerebro_url(@cerebro)
    assert_response :success
  end

  test "should update cerebro" do
    patch cerebro_url(@cerebro), params: { cerebro: { flavor: @cerebro.flavor } }
    assert_redirected_to cerebro_url(@cerebro)
  end

  test "should destroy cerebro" do
    assert_difference('Cerebro.count', -1) do
      delete cerebro_url(@cerebro)
    end

    assert_redirected_to cerebros_url
  end
end
